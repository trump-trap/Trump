//  g++ -std=c++17 -o tevent tevnet.cpp netlib.cpp BaseSocket.cpp util.cpp Lock.cpp EventDispatch.cpp -lpthread -Wno-deprecated
#include "netlib.h"

#include <iostream>
#include <cstring>

void conn_callback(void *callback_data, uint8_t msg, uint32_t handle, void *pParam)
{

    char buf[1024] {0};
    // int rear = 0;

    switch (msg)
    {
    case NETLIB_MSG_CONFIRM:
        //....
        break;
    case NETLIB_MSG_READ:
        netlib_recv(handle, buf, 1024);
        // rear = strlen(buf);
        // buf[rear - 1] = '\0';   
        std::cout << buf;
        netlib_send(handle, buf, strlen(buf));
        break;
    case NETLIB_MSG_WRITE:
        break;
    case NETLIB_MSG_CLOSE:
        std::cout << "client " << handle << " quit" << std::endl; 
        break;
    default:
        printf("!!!imconn_callback error msg: %d ", msg);
        break;
    }

}

void listen_callback(void *callback_data, uint8_t msg, uint32_t handle, void *pParam)
{
    if (msg == NETLIB_MSG_CONNECT)
    {
        std::cout << "connection coming." << std::endl;

        // conn_callback custom-made socket events operation.
        netlib_option(handle, NETLIB_OPT_SET_CALLBACK, (void *)conn_callback);
    }
    else
    {
        std::cout << "!!! listen error msg: " <<  msg <<  std::endl;
    }
}

int main(void)
{

    int ret = netlib_init();

    if (ret == NETLIB_ERROR) {
        std::cout << "netlib init error" << std::endl;
        return ret;
    }

    ret = netlib_listen("127.0.0.1", 10600, listen_callback, NULL);
    if (ret == NETLIB_ERROR){
        std::cout << "netlib listen error" << std::endl;
        return ret;
    }

    if (NETLIB_ERROR == ret)
    {
        std::cout << "netlib init error" << std::endl;
        return -2;
    }

    printf("server start listen on: %s:%d\n", "127.0.0.1", 10600);
    printf("now enter the event loop...\n");
    netlib_eventloop(10);

    ret = netlib_destroy();
    if (NETLIB_ERROR == ret)
    {
        std::cout << "netlib init error" << std::endl;
        return -3;
    }

    return 0;
}